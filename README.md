# Real Time Sense IoT Client Device (Raspberry Pi)

Python console application for a Real Time Sense Rasberry Pi device. Allows configuring the sensors, interfacing with the device, and setting up telemetry streams.

## Installation

### Windows

1. Download (clone) and unzip this repository.
2. Double click on run.bat to run the application.

### Linux

1. Download and unzip this repository.
2. Nagivate to unzipped folder in terminal.
3. type 'chmod +x run.sh' into the terminal.
4. type 'sh run.sh' into the terminal to run the application.

## User Guide

The application features built in help for each of the available commands, type **?** to see the help menu.

#### Setting up a device

1. run the application by following the instructions in the Installation section.
2. type **setup**

The application will automatically assign a **device_id** and **device_key** to the new device and will print them out. Note: When you're turning on a previously registered device, setup will load the previous settings, including the **device_id** and **device_key**.

3. Log in to the Real Time Sense web application.
4. Navigate to the team and project you want to add the device to and click on **add device**.
5. Copy and paste the **device_id** and **device_key** into the text fields to authenticate and register the device with the project.

#### Adding a sensor to your device

This will add a sensor to the device.  The next time the device broadcasts data to the cloud the new sensor will show up on your project web page.

1. type **sensor add [sensor_name] [sensor_unit]**.

#### Removing a sensor from your device

This wil remove a sensor from the device. you do not need to worry about losing previously stored data, although predictions that use the removed sensor will not use any newly recorded data until a sensor with the same name is added again.

1. type **sensor remove [sensor_name] [sensor_unit]**.

