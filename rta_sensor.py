import rta_client
import random
import json
import time

current_milli_time = lambda: int(round(time.time() * 1000))

is_gpio_supported = True
try:
    pass
    #import RPi.GPIO as GPIO
    #raise RuntimeError('')
except RuntimeError:
    print('GPIO either not supported on this platform or you need to run script with sudo access')
    is_gpio_supported = False

class RtaSensor:
    client = None
    name = ""
    unit = ""
    data = 0
    rnd = random.Random()
    gpio_channels = [26, 19, 13, 6, 5, 12, 16, 20]
    data_buffer = []
    last_sample_time = current_milli_time()

    def __init__(self, client, name, unit):
        self.client = client
        self.name = name
        self.unit = unit

        # if is_gpio_supported:
        #     GPIO.setmode(GPIO.BCM)
        #     for gpio_pin in self.gpio_channels:
        #         GPIO.setup(gpio_pin, GPIO.IN)

    def getJsonForSensorData(self, sample_time, pretty_sample_time):
        data_after_sample_time = 0

        valid_data = False
        remove_from_1_in_buffer = 0
        interpolated_data_value = 0

        for i in range(len(self.data_buffer)-1, 0, -1):
            data_point = self.data_buffer[i]
            prev_data_point = self.data_buffer[i - 1]
            if (data_point[0] > sample_time and prev_data_point[0] < sample_time):
                interpolated_data_value = self.interpolateDataValue(prev_data_point, data_point, sample_time)
                valid_data = True
                remove_from_1_in_buffer = i - 2
                break
        
        if (valid_data):
            for i in range(remove_from_1_in_buffer, -1, -1):
                self.data_buffer.pop(i)

        print("Time: {}\t\tValue: {}\t\t Data Buf Len: {}".format(pretty_sample_time, self.data, len(self.data_buffer)))
        data_payload = "\"{}\" : {} \"sensor_data\" : \"{}\", \"unit\" : \"{}\", \"sample_time\" : \"{}\" {}".format(self.name, '{', self.data, self.unit, pretty_sample_time, '}')
        return data_payload

    def interpolateDataValue(self, data1, data2, sample_time):
        data_time_difference = data2[0] - data1[0]
        sample_time_difference = sample_time - data1[0]
        data_change = data2[1] - data1[1]
        rate_of_change = data_change / data_time_difference

        return rate_of_change * sample_time_difference

    def sampleData(self):
        if is_gpio_supported:
            self.getGPIOSensorData()
        else:
            self.getRandomData()

        sampled_at_time = current_milli_time()
        self.data_buffer.append((sampled_at_time, self.data))  

    def getRandomData(self):
        self.data = self.rnd.randint(0, 100)

    def getGPIOSensorData(self):
        self.data = 0
        i = 0;
        for gpio_pin in self.gpio_channels:
            gpio_value = GPIO.input(gpio_pin)
            self.data = self.data + (gpio_value << i)
            i = i + 1
        
