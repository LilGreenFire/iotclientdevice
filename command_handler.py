import six.moves

setup = False

def help_command(rtaClient, args):
    print("")
    print("{:->80}".format(""))
    print("{:<30} - {}".format("setup", "Initializes the device or loads up device state of previously initialized device after being restarted."))
    print("{:<30} - {}".format("sensors", "Lists all of the currently configured sensors on the device."))
    print("{:<30} - {}".format("sensor add <name> [unit]", "Adds a new sensor to the device with the given name and an optional unit postfix."))
    print("{:<30} - {}".format("sensors start", "Begins reading all sensors data and posting them to the cloud (provided device has been created on Real Time Sense web app)"))
    print("{:<30} - {}".format("sensors stop", "Stops reading all sensors data and posting them to the cloud"))
    print("{:<30} - {}".format("?", "Prints help"))
    print("{:<30} - {}".format("q", "Saves device state and exits"))
    print("{:->80}".format(""))
    print("")

def setup_command(rtaClient, args):
    global setup
    rtaClient.setup()
    setup = True

def test_command(rtaClient, args):
    rtaClient.publish_message("test")

def quit_command(rtaClient, args):
    return False

def sensor_command(rtaClient, args):
    global setup
    if not setup:
        print("Device must be setup before interacting with sensors (use 'setup' command)")
        return
    if (len(args) == 0):
        print("\nSensors List")
        print("{:->80}".format(""))
        print("{name:{c}<{n}}{data:{c}<{n}}".format(name = "Name", data = "Data", c = ' ', n = 20))
        print("{:->80}".format(""))
        for sensor in rtaClient.sensors:
            data_with_unit = str(sensor.data) + sensor.unit
            print("{name:{c}<{n}}{data:{c}<{n}}".format(name = sensor.name, data =  data_with_unit, c = ' ', n = 20))
        print("{:->80}".format(""))

    elif (len(args) == 1 and args[0] == "send"):
        rtaClient.send_all_sensor_data()

    elif (len(args) == 1 and args[0] == "start"):
        rtaClient.start_reading_sensors_in_loop()

    elif (len(args) == 1 and args[0] == "stop"):
        print("Stopping sensor read loop")
        rtaClient.stop_reading_sensors_in_loop()
    
    elif (len(args) >= 2 and args[0] == "add"):
        unit_name = ""
        if (len(args) == 3):
            unit_name = args[2]
        if rtaClient.add_sensor(args[1], unit_name):
            print("'{}' sensors added to device".format(args[1]))

    elif (len(args) >= 2 and args[0] == "remove"):
        for sensor in rtaClient.sensors:
            if (sensor.name == args[1]):
                rtaClient.remove_sensor(args[1])
                return
        print("Sensor {} does not exist on device".format(args[1]))
        
